require('../connect')
var mongoose=require('mongoose')
var Schema=mongoose.Schema
var singerSchema=new Schema({
    name:String,
    sex:String,
    fannum:String,
    putername:String,
    style:String,
    address:String
})
var singerModel=mongoose.model('vocal',singerSchema,"vocal")

module.exports=singerModel
