require('../connect')
var mongoose=require('mongoose')
var Schema=mongoose.Schema
var userSchema=new Schema({
    username:{
        type:String,        
        unique:true,
        required:true,
        match:/^[a-zA-Z]([a-zA-Z0-9]{5,10})$/, //6至11位，以字母开头，字母+数字
      },
    password:{
        type:String,
        required:true,
        match:/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,16}$/ //至少8个字符，至少1个字母(大小写都可)和1个数字,不能包含特殊字符（非数字字母）
      },
    headphoto:String,
    likemusic:[{
        songname: String
    }]
})
var UserModel=mongoose.model('userList',userSchema,"userList")

module.exports=UserModel
