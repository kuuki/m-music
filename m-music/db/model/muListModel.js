require('../connect')
var mongoose=require('mongoose')
var Schema=mongoose.Schema
var songListSchema=new Schema({
    songname:String,
    singername:{
    type:String,
    required:true,
    unique:true
    },
    singerid:String,
    picturename:String,
    songstyle:String,
    list:String,
    clicknum:String
})
var songListModel=mongoose.model('song',songListSchema,"song")

module.exports=songListModel
