var express = require('express');
var router = express.Router();
var MuListModel = require('./../db/model/muListModel');

/* GET users listing. */
router.get('/', function(req, res, next) {
    // console.log(req.query);
    var singername = req.query.singername
    MuListModel.find({singername}, function(err, docs){
        // console.log(docs);
        res.send(docs)
    })
});

router.get('/playM', function(req, res){
    // console.log(req.query);
    var singername = req.query.singername
    var songname = req.query.songname
    // var arr = [] // 装歌曲
    MuListModel.findOne({songname},function(err, docs){
        if(err){
            console.log(err);
        }else{
            if(docs){
                res.send(docs)
            }else{
                res.send({
                    'code':'200',
                    'msg':'无歌曲'
                })
            }
        }
    })
})

router.get('/getList', function(req, res){
    // console.log(req.query);
    var singername = req.query.singername
    MuListModel.find({singername},function(err, docs){
        res.send(docs)
    })
})

router.get('/download', function(req, res, next) {
    console.log(req.query,222222222);
    var songname = req.query.songname
    // console.log(songname);
    console.log(`public/media/${songname}.mp3`,333);
    res.download(`public/media/${songname}.mp3`,function(err){
        if(err){
            console.log(err);
        }
    })
})
router.get('/transceiver', function(req, res){
    // console.log(req.query);
    MuListModel.find({},function(err, docs){
        res.send(docs)
    }).skip(5).limit(5)
})
router.get('/transceiver1', function(req, res){
    // console.log(req.query);
    MuListModel.find({},function(err, docs){
        res.send(docs)
    }).skip(10).limit(7)
  
})
router.get('/transceiver2', function(req, res){
    // console.log(req.query);
    MuListModel.find({},function(err, docs){
        res.send(docs)
    }).skip(17).limit(20)
  
})
module.exports = router;
