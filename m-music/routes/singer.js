var express = require('express');
var router = express.Router();
var SingerModel = require('./../db/model/singerModel');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.send('its songer')
});

// 初始化
router.get('/singerInit', function(req, res, next) {
  console.log('访问到singerInit');
  SingerModel.find({},function(err, docs){
      if(!err){
        res.send(docs);
      }else{
          console.log(err);
      }
  })
});

// 条件查询
router.get('/changeType', function(req,res){
  console.log(req.query);
  var address = req.query.countryType
  var sex = req.query.sexType
  var style = req.query.songType
  console.log(address,sex,style);
  var obj = {}
  if(address!='undefined'){
    obj.address=address
  }
  if(sex!='undefined'){
    obj.sex=sex
  }
  if(style!='undefined'){
    obj.style=style
  }
  console.log(obj);
  SingerModel.find(obj,function(err,docs){
    // console.log(docs);
    res.send(docs)
  })
})

// 查询
router.get('/select', function(req,res){
  console.log(req.query);
  var name = req.query.songername
  SingerModel.findOne({name},function(err,docs){
    // console.log(docs);
    res.send(docs)
  })
})

module.exports = router;
