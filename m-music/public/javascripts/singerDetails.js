var header = document.querySelector('header')
var h2 = header.querySelector('h2')
var bg = document.querySelector('.songerBox .section_01') // 需要改背景的元素
var singername = bg.querySelector('h3') // 需要渲染的名字
var fannum = bg.querySelector('.fannum')
var present = document.querySelector('.section_04 .comment') // 歌手简介
var singNum = document.querySelector('.singNum') // 歌曲数量
var songList = document.querySelector('.songList') // 歌曲

var songArr = ''
var sName = '' // 存放歌手名字
var difSong = ''

function init(){ 
    // var info = decodeURI(location.search)
    var info2 = sessionStorage.getItem('songername')
    console.log(info2,2222222222222222);
    // info = info.replace(/\?/,'')
    // console.log(info);
    var node = ''
    // var linkPage = info.match(/[^\&]+/g)
    // console.log(linkPage[1]);
    ajax({ // 从歌手页面来时 初始化
        url: 'singer/select', // 在歌手库里查找
        type: 'get',
        data: 'songername='+info2,
        dataType: 'json',
        success(res) {
            sName = res.name
            // console.log(sName);
            bg.style.background = `url('images/vocal/${res.putername}.jpg') no-repeat`
            bg.style.backgroundSize = `cover`
            singername.innerText = res.name
            fannum.innerText = res.fannum>10000?~~(res.fannum/10000):fannum
            present.innerText = res.present
            ajax({
                url: 'muList/',
                type: 'get',
                data: 'singername='+sName,
                dataType: 'json',
                success(res2) {
                    console.log(res2);
                    singNum.innerHTML = res2.length
                    res2.forEach(function(item,idx){
                        songArr += idx!=res2.length-1?item.songname+',':item.songname
                        node += `
                        <div class="item" value="${item.songname}" onclick="goPlayPage(event,this)">
                            <div class="msg">
                                <div class="name">${item.songname}</div>
                                <div class="style">${item.songstyle}</div>
                            </div>
                            <div class="btn">
                                <em class="fa fa-play-circle"></em>
                            </div>
                        </div>
                        `
                    })
                    difSong = node
                    songList.innerHTML = node
                }
            })
        }
    })  // ajax end
}
init()

// 翻页
var mainBox = document.querySelector('.mainBox')
var typeList = document.querySelector('.typeList')
var songerInfo = document.querySelector('.songerInfo')
// var classIndex = 0
var timer = null
var pageIndex = 1 // 默认在第一页
typeList.index = 1
songerInfo.index = 2
typeList.onclick = function(){
    changePage(this.index)
}
songerInfo.onclick = function(){
    changePage(this.index)
}
mainBox.ontouchstart = function(e){
    // e.stopPropagation()
    // e.preventDefault()
    // console.log(e);
    stA = mainBox.scrollLeft // 在x轴滑动的距离
    bginX = e.touches[0].pageX // 开始的距离
}
mainBox.ontouchmove = function(e){
    // e.stopPropagation()
    // e.preventDefault()
    stA = mainBox.scrollLeft
}
mainBox.ontouchend = function(e){
    // e.stopPropagation()
    // e.preventDefault()
    endX = e.changedTouches[0].pageX
    if(endX-bginX < -150){
        changePage(2)
        pageIndex = 2
    }else if(endX-bginX > 150){
        changePage(1)
        pageIndex = 1
    }else{
        if(pageIndex == 1){
            mainBox.scrollTo(0,0)   
        }else{
            mainBox.scrollTo(mainBox.offsetWidth,0)
        }
    }
    // mainBox.ontouchmove = function(){
    //     return false
    // }
}

// 监听滚动 改变header的透明度
document.addEventListener('scroll', function(){
    // console.log(window.scrollY);
    header.style.background = `rgba(50, 50, 50, ${window.scrollY/200})`
    h2.style.color = `rgba(255, 255, 255, ${window.scrollY/200})`
})




function changePage(idx){
    if(timer){
        return
    }
    // console.log(idx);
    var st = mainBox.scrollLeft
    var scW = mainBox.offsetWidth
    if(idx == 1){
        timer = setInterval(function(){
            mainBox.scrollTo(st,0)
            st-=35
            if(st <= -30){
                songerInfo.classList.remove('checked')
                typeList.classList.add('checked')
                clearInterval(timer)
                timer = null
            }
        },15)
    }
    if(idx == 2){
        timer = setInterval(function(){
            mainBox.scrollTo(st,0)
            st+=35
            if(st >= scW+30){
                typeList.classList.remove('checked')
                songerInfo.classList.add('checked')
                clearInterval(timer)
                timer = null
            }
        },15)
    }
}




// 点击显示 排序
var sortBtn = document.querySelector(".sort .btn")
var sortMask = document.querySelector(".pageMask")
var sortList = document.querySelector('.sortList')
var showText = document.querySelector('.showText')
var songListNew = document.querySelector('.songList')

var sortDif = sortMask.querySelector(".dif") // 默认排序
var sortWd = sortMask.querySelector(".wd") // 字母排序

sortBtn.onclick = function(){
    // console.log(difSong);
    sortMask.classList.remove('hid')
    setTimeout(function(){
        sortList.style.height = '4rem'
    },0)
}
sortMask.ontouchstart = function(){
    sortList.style.height = '0'
    setTimeout(function(){
        sortMask.classList.add('hid')
    },300)
}
sortDif.ontouchstart = function(){
    songListNew.innerHTML = difSong
    showText.innerText = '默认'
}
sortWd.ontouchstart = function(){
    defArr = [...songListNew.children] // 默认排序
    singNameArr = [...songListNew.children] // 歌名排序
    singNameArr.sort(function(a,b){
        if(a.getAttribute('value') > b.getAttribute('value')){
            return 1
        }else if(a.getAttribute('value') < b.getAttribute('value')){
            return -1
        }else{
            return 0
        }
    })
    var mod = ''
    singNameArr.forEach(function(item){
        mod += item.outerHTML
    })
    songListNew.innerHTML = mod
    showText.innerText = '字母'
}

function goPlayPage(event, that){
    // console.log(that);
    var songername = that.querySelector('.name').innerText
    // location.href = `playMusic.html?songstr=${songArr}&songname=${songername}`
    location.href = `playMusic.html`
    var msg = {
        "songstr":songArr,
        "songname":songername
    }
    console.log(msg);
    sessionStorage.setItem("msg",JSON.stringify(msg))
    event.stopPropagation()
}
mainBox.onwheel = function(){return false}
