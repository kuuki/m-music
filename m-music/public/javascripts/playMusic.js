var songname = document.querySelector('.singName')
var singerName = document.querySelector('.singerName')
var music = document.querySelector('.music')
var muBg = document.querySelector('.section_01 .content img')
var songArr = []
var clickMus = null
var isLike = false
var likeMusic = document.querySelector(".likeMusic")
var downloadMusic = document.querySelector('.downloadMusic')
var section_01 = document.querySelector('.section_01')

var linkMsg = null  // 分开存放search的内容
var index = 0  // 存放从上个页面点进来的歌是列表中的索引

var downMask = document.querySelector('.downMask')
var mainContent = document.querySelector('.section_01 .content')
var imgBox = document.querySelector('.imgBox') // 图片区域
var textBox = document.querySelector('.textBox') // 歌词区域


var musicBar = document.querySelector('.musicBar') // 总 进度条
var musicBefor = document.querySelector('.musicBefor') // 经过的进度条
var musicIcon = document.querySelector('.musicIcon') // 小圆点
var musicNowTime = document.querySelector('.musicNowTime') // 当前时间
var musicAllTime = document.querySelector('.musicAllTime') // 总时间
var volumeBar = document.querySelector('.volumeBar') // 音量条
var volumeBefor = document.querySelector('.volumeBefor') // 音量条
var volumeIcon = document.querySelector('.volumeIcon') // 音量键

var after = document.querySelector('.after') // 上一首
var play = document.querySelector('.play')  // 播放
var pause = document.querySelector('.pause') // 暂停
var next = document.querySelector('.next') // 下一首

var txt_mus = 0 // 进度条歌词表比例

// 渲染默认数据  需要获取到从哪个页面点进来的 和 歌曲列表(歌名) 和 点击的歌名
function init(){
    var info2 = JSON.parse(sessionStorage.getItem("msg"))
    console.log(info2);
    // var info = decodeURI(location.search)
    // info = info.replace(/\?/,'')
    // console.log(info);
    // linkMsg = info.match(/[^\&]+/g)
    // console.log(linkMsg);
    songArr = info2.songstr.split(",")
    // index = songArr.indexOf(linkMsg[1].replace(/([^&]+)=/,'')) // 设置索引
    index = songArr.indexOf(info2.songname) // 设置索引
    console.log('index',index);
    ajax({ // 渲染歌曲
        url: 'muList/playM',
        type: 'get',
        data: 'songname='+info2.songname,
        dataType: 'json',
        success(res) {
            console.log('init执行');
            clickMus = res
            songname.innerText = res.songname
            singerName.innerText = res.singername
            downMask.style.background = `url(images/song/${res.picturename}.jpg) no-repeat 50% 50% / cover`
            muBg.src = `images/song/${res.picturename}.jpg`
            music.src = `media/${res.songname}.mp3`
            downloadMusic.href = `media/${songname.innerText}.mp3`
            setLike(songname.innerText)
        }
    })
    getText(info2.songname)
    
}
init()
function getText(songname){ // 渲染歌词
    ajax({ 
        url:'/lyric',
        type:'get',
        data:'songname='+songname,
        dataType:'json',
        success(res){
            var textNode = ''
            console.log(res,11111111111111111111111111111);
            if(res == "缺少歌词"){
                textBox.innerHTML = `
                    <p style="margin: 50% 0 0 0">${res}</p>
                `
            }else{ // 不缺少歌词的话 获取 渲染
                var arr = res.text.split(' ')
                // console.log(arr);
                arr.forEach(function(item){
                    textNode += `
                        <p>${item}</p>
                    `
                })
                textBox.innerHTML = textNode
                // 渲染完歌词后 获取进度条与歌词表的长度比例
                txt_mus = (textBox.scrollHeight-textBox.offsetHeight) / (musicBar.offsetWidth)
            }
        }
    })
}

var timerMain = null
// 点击中间区域 切换显示
mainContent.onclick = function(){
    if(timerMain){
        return
    }
    timerMain = setTimeout(function(){
        imgBox.classList.toggle('op0')
        textBox.classList.toggle('op0')
        timerMain = null
    },260)
}
// imgBox.onclick = function(){
//     console.log(1111111);
// }
// textBox.onclick = function(){
//     console.log(2222222);
// }


// 设置点击收藏歌曲
likeMusic.onclick = function(){
    console.log('点击收藏',isLike);
    if(isLike==true || isLike=='true'){ // 为真 点击变灰 删除数据库中的收藏
        this.classList.remove('fa-heart')
        this.classList.add("fa-heart-o")
        this.style.color = "#dedede"
        isLike = false
        ajax({
            url: 'userList/removeLike',
            type: 'get',
            data: 'songname='+songname.innerText,
            success(res) {
                console.log(res);
            }
        })
    }else if(isLike==false || isLike=='false'){
        this.classList.remove('fa-heart-o')
        this.classList.add("fa-heart")
        this.style.color = "red"
        isLike = true
        ajax({
            url: 'userList/addLike',
            type: 'get',
            data: 'songname='+songname.innerText,
            success(res) {
                console.log(res);
            }
        })
    }
    likeMusic.style.transform = 'scale(1.5)'
    likeMusic.addEventListener('transitionend',function(){
        likeMusic.style.transform = 'scale(1)'
    })
}

// downloadMusic.onclick = function(){
//     console.log(songname.innerText);
// }




var turn = 0
var timer = null

music.oncanplaythrough = function(){
    var all = Math.floor(music.duration)
    musicAllTime.innerHTML = getTime(all)
}


function anime(){
    turn+=.25
    muBg.style.transform = `rotate(${turn}deg)`
}
// 改变的时候
music.ontimeupdate=function(){
    // 播放完成 暂停
    if(musicNowTime.innerHTML == musicAllTime.innerHTML){
        pause.click()
    }
    // console.dir(this.currentTime);
    var ratio = this.currentTime / this.duration
    var nowX = ratio * musicBar.offsetWidth
    musicIcon.style.left = nowX-6 + 'px'
    musicBefor.style.width = nowX + 'px'
    musicNowTime.innerHTML = getTime(Math.floor(music.currentTime))
    // 改变歌词的滚动距离
    // console.log(musicBefor.offsetWidth);
    textBox.scrollTo(0,txt_mus*musicBefor.offsetWidth)
}


play.onclick = function(){
    music.play()
    this.classList.add('hid')
    pause.classList.remove('hid')
    timer = setInterval(anime,16)
}
pause.onclick = function(){
    music.pause()
    this.classList.add('hid')
    play.classList.remove('hid')
    clearInterval(timer)
}
after.onclick = function(e){ // 上一首
    e.stopPropagation()
    index--
    if(index < 0){
        index = songArr.length-1
    }
    changeMusic()
    checkSong()
    // setLike(songArr[index].songname)
}
next.onclick = function(e){ // 下一首
    e.stopPropagation()
    index++
    if(index > songArr.length-1){
        index = 0
    }
    changeMusic()
    checkSong()
}
function changeMusic(){
    ajax({
        url: 'muList/playM',
        type: 'get',
        data: 'songname='+songArr[index],
        dataType: 'json',
        success(res) {
            console.log(res);
            clickMus = res
            // console.log(res,index);
            songname.innerText = res.songname
            singerName.innerText = res.singername
            downMask.style.background = `url(images/song/${res.picturename}.jpg) no-repeat 50% 50% / cover`
            muBg.src = `images/song/${res.picturename}.jpg`
            music.src = `media/${res.songname}.mp3`
            downloadMusic.href = `media/${res.songname}.mp3`
            setLike(res.songname)
            getText(res.songname)
        }
    })
}
// 歌曲进度条
musicIcon.ontouchstart = function(e){
    var ratio = 0
    musicIcon.ontouchmove = function(mvE){
        var moveX = mvE.touches[0].pageX - musicBar.offsetLeft
        if(moveX >= musicBar.offsetWidth){
            moveX = musicBar.offsetWidth
        }
        if(moveX <= 0){
            moveX = 0
        }
        ratio = (moveX) / (musicBar.offsetWidth)
        music.currentTime = music.duration * ratio
        musicBefor.style.width = moveX + 'px'
        musicIcon.style.left = moveX-6 + 'px'
    }
    e.stopPropagation()
}
// 声音
volumeIcon.ontouchstart = function(e){
    var ratio = 0
    // icon 拖动的时候 改变进度条
    e.stopPropagation() // 阻止事件冒泡
    document.ontouchmove = function(mvE){
        // console.log(music.volume);
        var moveX = mvE.touches[0].pageX - musicBar.offsetLeft - 4
        // 边界判断
        if(moveX >= volumeBar.offsetWidth - 4){
            moveX = volumeBar.offsetWidth - 4
        }
        if(moveX <= 0){
            moveX = 0
        }
        ratio = moveX / (musicBar.offsetWidth -4)
        music.volume = ratio
        volumeBefor.style.width = moveX + 'px'
        volumeIcon.style.left = moveX + 'px'
    }
}
document.ontouchend = function(){
    document.ontouchmove = null
}

// 把秒数转换为分秒
function getTime(sec){
    var mm = Math.floor(sec/60)
    if(sec < 60){
        if(sec < 10){
            return '0:0' + sec
        }else{
            return '0:' + sec
        }
    }else{
        if(sec%60 < 10){
            return `${mm}:0${sec%60}`
        }else{
            return `${mm}:${sec%60}`
        }
    }
}
// 切歌时 初始化
function checkSong(){ 
    songname.innerText = songArr[index].songname
    singerName.innerText = songArr[index].singername
    // muBg.src = `images/song/${songArr[index].picturename}.jpg`
    // music.src = `media/${songArr[index].songname}.mp3`
    pause.click()
    music.currentTime = 0
    musicIcon.style.left = 0
    musicBefor.style.width = 0
}

// 设置是否收藏歌曲
function setLike(nName){
    ajax({
        url: '/UserList',
        type: 'get',
        data: 'songname='+nName,
        success(res){
            console.log('ressss',res);
            isLike = res
            // console.log('islike',isLike);
            if(isLike == 'true'){
                // console.log('设置成true样式',isLike);
                likeMusic.classList.remove('fa-heart-o')
                likeMusic.classList.add("fa-heart")
                likeMusic.style.color = "red"
                // isLike = false
            }else{
                // console.log('设置成false样式',isLike);
                likeMusic.classList.remove('fa-heart')
                likeMusic.classList.add("fa-heart-o")
                likeMusic.style.color = "#dedede"
                // isLike = true
            }
        }
    })
}



var backSinger = document.querySelector('.backSinger')
backSinger.onclick = function(event){
    // location.href = `${linkMsg[0].replace(/([^&]+)=/,'')}?songername=${singerName.innerText}`
    window.history.back()
    event.stopPropagation()
}