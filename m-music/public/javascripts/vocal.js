var banner = document.querySelector('.banner')
var allList = document.querySelector('.section_03 .content')
function init(){ // 初始化数据
    ajax({
        url: 'singer/singerInit',
        type: 'get',
        dataType: 'json',
        success(res) {
            // console.log(res);
            var bannerItem = ''
            var allItem = ''
            for(var i=0; i<6; i++){
                bannerItem += `
                <div class="vocal_item dis-flex dir-column jus-center ali-center" onclick="changePage(event,this)">
                    <img src="images/vocal/${res[i].putername}.jpg">
                    <span class="name">${res[i].name}</span>
                    <div class="play_icon fa fa-play-circle"></div>
                </div>
                `
            }
            banner.innerHTML = bannerItem
            res.forEach(function(item){
                allItem += `
                    <div class="item" onclick="changePage(event,this)">
                        <div class="singerMsg">
                            <img src="images/vocal/${item.putername}.jpg">
                            <div class="msg">
                                <span class="name">${item.name}</span>
                                <span class="fans">粉丝数:${item.fannum>10000?~~(item.fannum/10000)+'万':item.fannum}</span>
                            </div>
                        </div>
                        <div class="btn">
                            <span>+关注</span>
                        </div>
                    </div>
                `
            })
            allList.innerHTML = allItem
        }
    })
    // console.log(banner,allList);
}
init()


// 点击分类
var countryType = document.querySelector('.countryType')
var sexType = document.querySelector('.sexType')
var songType = document.querySelector('.songType')
var counIndex = 0
var sexIndex = 0
var songIndex = 0
var checked = {
    // countryType:'全部',
    // sexType:'全部',
    // songType:'全部'
}
var typeItem = ''
for(var i in countryType.children){
    countryType.children[i].index = i
    countryType.children[i].ontouchstart = function(){
        var index = this.index
        countryType.children[counIndex].classList.remove('checked')
        this.classList.add('checked')
        counIndex = index
        checked.countryType = this.getAttribute('value')
        if(this.getAttribute('value')=='全部'){ // 如果是全部 则删除这个属性
            delete checked.countryType
        }
        console.log(checked);
        ajax({
            url: 'singer/changeType',
            type: 'get',
            data: `countryType=${checked.countryType}&sexType=${checked.sexType}&songType=${checked.songType}`,
            dataType: 'json',
            success(res){
                console.log(res);
                res.forEach(function(item){
                    typeItem+=`
                        <div class="item" onclick="changePage(event,this)">
                            <div class="singerMsg">
                                <img src="images/vocal/${item.putername}.jpg">
                                <div class="msg">
                                    <span class="name">${item.name}</span>
                                    <span class="fans">粉丝数:${item.fannum>10000?~~(item.fannum/10000)+'万':item.fannum}</span>
                                </div>
                            </div>
                            <div class="btn">
                                <span>+关注</span>
                            </div>
                        </div>
                    `
                })
                allList.innerHTML = typeItem
                typeItem=''
            }
        })
    }
}
for(var i in sexType.children){
    sexType.children[i].index = i
    sexType.children[i].ontouchstart = function(){
        var index = this.index
        sexType.children[sexIndex].classList.remove('checked')
        this.classList.add('checked')
        sexIndex = index
        checked.sexType = this.getAttribute('value')
        if(this.getAttribute('value')=='全部'){ // 如果是全部 则删除这个属性
            delete checked.sexType
        }
        console.log(checked);
        ajax({
            url: 'singer/changeType',
            type: 'get',
            data: `countryType=${checked.countryType}&sexType=${checked.sexType}&songType=${checked.songType}`,
            dataType: 'json',
            success(res){
                console.log(res);
                res.forEach(function(item){
                    typeItem+=`
                        <div class="item" onclick="changePage(event,this)">
                            <div class="singerMsg">
                                <img src="images/vocal/${item.putername}.jpg">
                                <div class="msg">
                                    <span class="name">${item.name}</span>
                                    <span class="fans">粉丝数:${item.fannum>10000?~~(item.fannum/10000)+'万':item.fannum}</span>
                                </div>
                            </div>
                            <div class="btn">
                                <span>+关注</span>
                            </div>
                        </div>
                    `
                })
                allList.innerHTML = typeItem
                typeItem=''
            }
        })
    }
}
for(var i in songType.children){
    songType.children[i].index = i
    songType.children[i].ontouchstart = function(){
        var index = this.index
        songType.children[songIndex].classList.remove('checked')
        this.classList.add('checked')
        songIndex = index
        checked.songType = this.getAttribute('value')
        if(this.getAttribute('value')=='全部'){ // 如果是全部 则删除这个属性
            delete checked.songType
        }
        console.log(checked);
        ajax({
            url: 'singer/changeType',
            type: 'get',
            data: `countryType=${checked.countryType}&sexType=${checked.sexType}&songType=${checked.songType}`,
            dataType: 'json',
            success(res){
                console.log(res);
                res.forEach(function(item){
                    typeItem+=`
                        <div class="item" onclick="changePage(event,this)">
                            <div class="singerMsg">
                                <img src="images/vocal/${item.putername}.jpg">
                                <div class="msg">
                                    <span class="name">${item.name}</span>
                                    <span class="fans">粉丝数:${item.fannum>10000?~~(item.fannum/10000)+'万':item.fannum}</span>
                                </div>
                            </div>
                            <div class="btn">
                                <span>+关注</span>
                            </div>
                        </div>
                    `
                })
                allList.innerHTML = typeItem
                typeItem=''
            }
        })
    }
}

function changePage(event,that){
    console.log(that);
    var songername = that.querySelector('.name').innerText
    // location.href = 'singerDetails.html?linkPage=singer&songername='+songername
    location.href = 'singerDetails.html'
    sessionStorage.setItem('songername',songername)
    event.stopPropagation()
}



